# Copyright (C) 2018 Sebastian Khan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# define required parameters for time domain and frequency domain
# as well as their default values

import lal
import lalsimulation as lalsim

common_params = dict(
    mass1=50,
    mass2=50,
    spin1x=0,
    spin1y=0,
    spin1z=0,
    spin2x=0,
    spin2y=0,
    spin2z=0,
    distance=100,
    inclination=0,
    phiRef=0,
    longAscNodes=0,
    eccentricity=0,
    meanPerAno=0,
    f_min=20,
    f_ref=20,
    extra_params=None,
    approximant="IMRPhenomD"
)

td_params = dict(delta_t=1./2048, **common_params)

fd_params = dict(delta_f=1./4, f_max=1024.,  **common_params)

class Parameters(object):
    def __init__(self, domain, **kwargs):
        """
        domain == 'td' of 'fd'
        kwargs: extra parameters including the waveform generation parameters.
        Units are MSUN, Mpc, Hz.

        attrbutes: params: dictionary with waveform generator parameters
        use the @properties such as 'SimInspiralChooseTDWaveform_params'
        to return a dictionary that is directly compatible with SimInspiralChooseTDWaveform
        """
        self.domain = domain
        self.kwargs = kwargs

        self.params = {} # stores wf generator parameters
        # first lets populate with the default parameters
        if self.domain == 'td':
            for k,v in td_params.iteritems():
                self.params.update({k:v})
        elif self.domain == 'fd':
            for k,v in fd_params.iteritems():
                self.params.update({k:v})


        # now override default parameters
        if self.domain == 'td':
            for k,v in kwargs.iteritems():
                if k in td_params.keys():
                    self.params.update({k:v})
                else:
                    msg="{} not recognised".format(k)
                    raise ValueError(msg)

        if self.domain == 'fd':
            for k,v in kwargs.iteritems():
                if k in fd_params.keys():
                    self.params.update({k:v})
                else:
                    msg="{} not recognised".format(k)
                    raise ValueError(msg)

        pass

    @property
    def SimInspiralChooseTDWaveform_params(self):
        assert self.domain == 'td', "SimInspiralChooseTDWaveform_params only works for domain='td'"
        pars = {
            'm1':self.params['mass1']*lal.MSUN_SI,
            'm2':self.params['mass2']*lal.MSUN_SI,
            's1x':self.params['spin1x'],
            's1y':self.params['spin1y'],
            's1z':self.params['spin1z'],
            's2x':self.params['spin2x'],
            's2y':self.params['spin2y'],
            's2z':self.params['spin2z'],
            'distance':self.params['distance']*1e6*lal.PC_SI,
            'inclination':self.params['inclination'],
            'phiRef':self.params['phiRef'],
            'longAscNodes':self.params['longAscNodes'],
            'eccentricity':self.params['eccentricity'],
            'meanPerAno':self.params['meanPerAno'],
            'deltaT':self.params['delta_t'],
            'f_min':self.params['f_min'],
            'f_ref':self.params['f_ref'],
            'params':self.params['extra_params'],
            'approximant':lalsim.GetApproximantFromString(self.params['approximant'])
        }
        return pars

    @property
    def SimInspiralTD_params(self):
        assert self.domain == 'td', "SimInspiralTD_params only works for domain='td'"
        pars = {
            'm1':self.params['mass1']*lal.MSUN_SI,
            'm2':self.params['mass2']*lal.MSUN_SI,
            'S1x':self.params['spin1x'],
            'S1y':self.params['spin1y'],
            'S1z':self.params['spin1z'],
            'S2x':self.params['spin2x'],
            'S2y':self.params['spin2y'],
            'S2z':self.params['spin2z'],
            'distance':self.params['distance']*1e6*lal.PC_SI,
            'inclination':self.params['inclination'],
            'phiRef':self.params['phiRef'],
            'longAscNodes':self.params['longAscNodes'],
            'eccentricity':self.params['eccentricity'],
            'meanPerAno':self.params['meanPerAno'],
            'deltaT':self.params['delta_t'],
            'f_min':self.params['f_min'],
            'f_ref':self.params['f_ref'],
            'LALparams':self.params['extra_params'],
            'approximant':lalsim.GetApproximantFromString(self.params['approximant'])
        }
        return pars


    @property
    def SimInspiralChooseFDWaveform_params(self):
        assert self.domain == 'fd', "SimInspiralChooseFDWaveform_params only works for domain='fd'"
        pars = {
            'm1':self.params['mass1']*lal.MSUN_SI,
            'm2':self.params['mass2']*lal.MSUN_SI,
            'S1x':self.params['spin1x'],
            'S1y':self.params['spin1y'],
            'S1z':self.params['spin1z'],
            'S2x':self.params['spin2x'],
            'S2y':self.params['spin2y'],
            'S2z':self.params['spin2z'],
            'distance':self.params['distance']*1e6*lal.PC_SI,
            'inclination':self.params['inclination'],
            'phiRef':self.params['phiRef'],
            'longAscNodes':self.params['longAscNodes'],
            'eccentricity':self.params['eccentricity'],
            'meanPerAno':self.params['meanPerAno'],
            'deltaF':self.params['delta_f'],
            'f_min':self.params['f_min'],
            'f_max':self.params['f_max'],
            'f_ref':self.params['f_ref'],
            'LALpars':self.params['extra_params'],
            'approximant':lalsim.GetApproximantFromString(self.params['approximant'])
        }
        return pars


    @property
    def SimInspiralFD_params(self):
        assert self.domain == 'fd', "SimInspiralFD_params only works for domain='fd'"
        pars = {
            'm1':self.params['mass1']*lal.MSUN_SI,
            'm2':self.params['mass2']*lal.MSUN_SI,
            'S1x':self.params['spin1x'],
            'S1y':self.params['spin1y'],
            'S1z':self.params['spin1z'],
            'S2x':self.params['spin2x'],
            'S2y':self.params['spin2y'],
            'S2z':self.params['spin2z'],
            'distance':self.params['distance']*1e6*lal.PC_SI,
            'inclination':self.params['inclination'],
            'phiRef':self.params['phiRef'],
            'longAscNodes':self.params['longAscNodes'],
            'eccentricity':self.params['eccentricity'],
            'meanPerAno':self.params['meanPerAno'],
            'deltaF':self.params['delta_f'],
            'f_min':self.params['f_min'],
            'f_max':self.params['f_max'],
            'f_ref':self.params['f_ref'],
            'LALparams':self.params['extra_params'],
            'approximant':lalsim.GetApproximantFromString(self.params['approximant'])
        }
        return pars

