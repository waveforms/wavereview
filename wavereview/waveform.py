# Copyright (C) 2018 Sebastian Khan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import lal
import lalsimulation as lalsim

try:
    from pycbc.types import TimeSeries, FrequencySeries
except ImportError:
    pass

from parameters import Parameters

def get_lalsim_waveform(generator_function, cast_as_pycbc=True, **kwargs):
    """
    return waveform generated using lalsimulation

    generator_function: function
        choices = [SimInspiralChooseTDWaveform,
        SimInspiralChooseFDWaveform,
        SimInspiralFD,
        SimInspiralTD]

    kwargs: use this to pass parameters to the generator_function
    """

    td_functions = ['SimInspiralChooseTDWaveform', 'SimInspiralTD']
    fd_functions = ['SimInspiralChooseFDWaveform', 'SimInspiralFD']

    g_function_choices = td_functions + fd_functions

    if generator_function.__name__ not in g_function_choices:
        msg="generator_function = {} not supported ".format(generator_function.__name__)
        msg+="choose from {}".format(g_function_choices)
        raise ValueError(msg)

    if generator_function.__name__ in td_functions:
        domain='td'
    elif generator_function.__name__ in fd_functions:
        domain='fd'

    Pars = Parameters(domain=domain, **kwargs)

    if generator_function.__name__ == 'SimInspiralChooseTDWaveform':
        wf_pars = Pars.SimInspiralChooseTDWaveform_params

    if generator_function.__name__ == 'SimInspiralChooseFDWaveform':
        wf_pars = Pars.SimInspiralChooseFDWaveform_params

    if generator_function.__name__ == 'SimInspiralFD':
        wf_pars = Pars.SimInspiralFD_params

    if generator_function.__name__ == 'SimInspiralTD':
        wf_pars = Pars.SimInspiralTD_params

    hp1, hc1 = generator_function(**wf_pars)

    if cast_as_pycbc:
        if generator_function.__name__ in ['SimInspiralChooseTDWaveform', 'SimInspiralTD']:
            hp = TimeSeries(hp1.data.data[:], delta_t=hp1.deltaT, epoch=hp1.epoch)
            hc = TimeSeries(hc1.data.data[:], delta_t=hc1.deltaT, epoch=hc1.epoch)
        elif generator_function.__name__ in ['SimInspiralChooseFDWaveform', 'SimInspiralFD']:
            hp = FrequencySeries(hp1.data.data[:], delta_f=hp1.deltaF, epoch=hp1.epoch)
            hc = FrequencySeries(hc1.data.data[:], delta_f=hc1.deltaF, epoch=hc1.epoch)
    else:
        hp = hp1
        hc = hc1

    return hp, hc