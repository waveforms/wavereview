# Copyright (C) 2018 Sebastian Khan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from parameters import Parameters
import numpy as np
import os

memory_leak_template_script="""

# note this only works with C compiled code - not pip installed lalsuite.
# usage
# source memory_leak_check.sh &> memory_leak_check_output.log  &

valgrind --leak-check=full --show-leak-kinds=all  --leak-check=yes lalsim-inspiral --domain {domain} -a {approx} --m1 {mass1} --m2 {mass2} --inclination {iota} --spin1x {spin1x} --spin1y {spin1y} --spin1z {spin1z} --spin2x {spin2x} --spin2y {spin2y} --spin2z {spin2z} --f-min {f_min}

echo "Done!"
"""

valgrind_profiling_template_script="""

# note this only works with C compiled code - not pip installed lalsuite.

valgrind --tool=callgrind lalsim-inspiral --domain {domain} -a {approx} --m1 {mass1} --m2 {mass2} --inclination {iota} --spin1x {spin1x} --spin1y {spin1y} --spin1z {spin1z} --spin2x {spin2x} --spin2y {spin2y} --spin2z {spin2z} --f-min {f_min}

echo "Done!"
"""

def generate_memory_leak_check(domain, outdir, **kwargs):
    """
    domain = 'td' or 'fd'
    """
    Pars = Parameters(domain=domain, **kwargs)

    if domain == 'td':
        domain = 'time'
    elif domain == 'fd':
        domain = 'freq'

    lalsim_inspiral_pars={
        "domain":domain,
        "approx":Pars.params["approximant"],
        "mass1":Pars.params["mass1"],
        "mass2":Pars.params["mass2"],
        "iota":Pars.params["inclination"] * 180 / np.pi, # lalsim-inspiral is in degrees
        "spin1x":Pars.params["spin1x"],
        "spin1y":Pars.params["spin1y"],
        "spin1z":Pars.params["spin1z"],
        "spin2x":Pars.params["spin2x"],
        "spin2y":Pars.params["spin2y"],
        "spin2z":Pars.params["spin2z"],
        "f_min":Pars.params["f_min"]
    }


    s=memory_leak_template_script.format(**lalsim_inspiral_pars)
    name = os.path.join(outdir, "memory_leak_check.sh")
    f=open(name, "w")
    f.write(s)
    f.close()

def generate_profiling_check(domain, outdir, **kwargs):
    """
    domain = 'td' or 'fd'
    """
    Pars = Parameters(domain=domain, **kwargs)

    if domain == 'td':
        domain = 'time'
    elif domain == 'fd':
        domain = 'freq'

    lalsim_inspiral_pars={
        "domain":domain,
        "approx":Pars.params["approximant"],
        "mass1":Pars.params["mass1"],
        "mass2":Pars.params["mass2"],
        "iota":Pars.params["inclination"] * 180 / np.pi, # lalsim-inspiral is in degrees
        "spin1x":Pars.params["spin1x"],
        "spin1y":Pars.params["spin1y"],
        "spin1z":Pars.params["spin1z"],
        "spin2x":Pars.params["spin2x"],
        "spin2y":Pars.params["spin2y"],
        "spin2z":Pars.params["spin2z"],
        "f_min":Pars.params["f_min"]
    }


    s=valgrind_profiling_template_script.format(**lalsim_inspiral_pars)
    name = os.path.join(outdir, "profiling_check.sh")
    f=open(name, "w")
    f.write(s)
    f.close()