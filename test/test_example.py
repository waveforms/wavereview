# Copyright (C) 2018 Sebastian Khan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from pycbc.filter import overlap

import copy

import lal
import lalsimulation as lalsim

from wavereview.waveform import get_lalsim_waveform
import wavereview.parameters

def almost_equal(x,y,threshold=0.0001):
  return abs(x-y) < threshold


# def test_polarisation():
#     """
#     test that hp and hc have the correct polarisaiton convention
#     that we use in lalsimulation.
#     i.e. when inc=0 hp leads hc and
#     when inc=pi hc leads hp (or whatever the correct convention is!)
#     """
#     raise NotImplementedError

# def test_distance_scale():

#     raise NotImplementedError

def test_swapping_constituents_non_spin(approx="EOBNRv2HM"):
    """
    for non-spinning waveforms if you swap the masses and spins
    you should get the same waveform
    """

    generator_function = lalsim.SimInspiralChooseTDWaveform

    Pars = wavereview.parameters.Parameters(domain='td')


    #FIXME: add phiRef to this?

    Pars.params.update({
        "mass1":50,
        "mass2":30,
        "approximant":approx
    })

    hp, hc = get_lalsim_waveform(generator_function, cast_as_pycbc=True, **Pars.params)

    params_swap = copy.copy(Pars.params)
    params_swap.update({
        "mass1":Pars.params["mass2"],
        "mass2":Pars.params["mass1"],
        "approximant":approx
    })

    hp_swap, hc_swap = get_lalsim_waveform(generator_function, cast_as_pycbc=True, **params_swap)

    op = overlap(hp, hp_swap)
    assert almost_equal( 1, op, threshold=0.0001 ) == True
    oc = overlap(hc, hc_swap)
    assert almost_equal( 1, op, threshold=0.0001 ) == True


def test_swapping_constituents_aligned_spin(approx="SEOBNRv4"):
    """
    for aligned spin waveforms if you swap the masses and spins
    you should get the same waveform
    """

    generator_function = lalsim.SimInspiralChooseTDWaveform

    Pars = wavereview.parameters.Parameters(domain='td')

    #FIXME: add phiRef to this?

    Pars.params.update({
        "mass1":50,
        "mass2":30,
        "spin1z":-0.234,
        "spin2z":0.972,
        "approximant":approx
    })

    hp, hc = get_lalsim_waveform(generator_function, cast_as_pycbc=True, **Pars.params)

    params_swap = copy.copy(Pars.params)
    params_swap.update({
        "mass1":Pars.params["mass2"],
        "mass2":Pars.params["mass1"],
        "spin1z":Pars.params["spin2z"],
        "spin2z":Pars.params["spin1z"],
        "approximant":approx
    })

    hp_swap, hc_swap = get_lalsim_waveform(generator_function, cast_as_pycbc=True, **params_swap)

    op = overlap(hp, hp_swap)
    assert almost_equal( 1, op, threshold=0.0001 ) == True
    oc = overlap(hc, hc_swap)
    assert almost_equal( 1, op, threshold=0.0001 ) == True

# def test_swapping_constituents_precessing(approx="SEOBNRv3"):
# def test_swapping_constituents_precessing(approx="SpinTaylorT4"):
# def test_swapping_constituents_precessing(approx="SpinTaylorT2"):
def test_swapping_constituents_precessing(approx="IMRPhenomPv2"):
    """
    for precessing spin waveforms if you swap the masses and spins
    you should get the same waveform
    """
    generator_function = lalsim.SimInspiralChooseTDWaveform

    Pars = wavereview.parameters.Parameters(domain='td')

    #FIXME: add phiRef to this?

    Pars.params.update({
        "mass1":50,
        "mass2":30,
        "spin1x":0.24,
        "spin1y":-0.12,
        "spin1z":-0.234,
        "spin2x":0.12,
        "spin2y":-0.42,
        "spin2z":0.51,
        "approximant":approx
    })

    hp, hc = get_lalsim_waveform(generator_function, cast_as_pycbc=True, **Pars.params)

    params_swap = copy.copy(Pars.params)
    params_swap.update({
        "mass1":Pars.params["mass2"],
        "mass2":Pars.params["mass1"],
        "spin1x":Pars.params["spin2x"],
        "spin1y":Pars.params["spin2y"],
        "spin1z":Pars.params["spin2z"],
        "spin2x":Pars.params["spin1x"],
        "spin2y":Pars.params["spin1y"],
        "spin2z":Pars.params["spin1z"],
        "approximant":approx
    })

    hp_swap, hc_swap = get_lalsim_waveform(generator_function, cast_as_pycbc=True, **params_swap)


    op = overlap(hp, hp_swap)
    assert almost_equal( 1, op, threshold=0.0001 ) == True
    oc = overlap(hc, hc_swap)
    assert almost_equal( 1, op, threshold=0.0001 ) == True