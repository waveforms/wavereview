#!/bin/bash

unset PYTHONPATH
virtualenv venv
source venv/bin/activate
mkdir -p $VIRTUAL_ENV/src
cd $VIRTUAL_ENV/src
git clone git@github.com:gwastro/pycbc.git
cd pycbc
pip install --upgrade pip
pip install -e .
pip install -r requirements.txt
pip install --upgrade ipython
pip install lalsuite
