#!/usr/bin/env python
#
# Copyright (C) 2018 Sebastian Khan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from distutils.core import setup
from setuptools import find_packages

setup(name='wavereview',
      version='0.0.1',
      description='Package to help review waveforms',
      author='Sebastian Khan',
      author_email='sebastian.khan@ligo.org',
      packages=find_packages(),
      url='https://git.ligo.org/waveforms/wavereview',
      download_url='https://git.ligo.org/waveforms/wavereview',
      install_requires=[
        'h5py',
        'numpy',
        'scipy',
        'matplotlib',
        'pytest'],
      scripts = [
        'bin/wavereview_review'
      ],
      license='GPLv3')

